extends Node

var pr_brick = preload("res://prefab/game/brick.xscn") 

var bonus = [
	preload("res://prefab/game/bonus/inc_bat.xscn"),
	preload("res://prefab/game/bonus/dec_bat.xscn"),
	preload("res://prefab/game/bonus/super_ball.xscn"),
	preload("res://prefab/game/bonus/add_life.xscn"),
	preload("res://prefab/game/bonus/dead.xscn"),
]

export var col = 10
export var row = 6
export var top_offset = 64
export var brick_offset = Vector2(64, 32)

var start = Vector2(0, 0)

var total_bricks = 0
var destroy_bricks = 0
var screen_rect = null

var game = null

func get_bonus():
	if randf() < 0.85:
		return null
	
	var idx = randi() % bonus.size()
	var inst = bonus[idx];
	
	return inst.instance()


func create():
	var sx = (screen_rect.size.width - brick_offset.x*col)/2 + brick_offset.x/2
	start = Vector2(sx, top_offset)

	var color_offset = randi()

	for y in range(row):
		for x in range(col):
			var brick = pr_brick.instance()
			var pos = Vector2(x*brick_offset.x, y*brick_offset.y) + start
			brick.set_pos(pos)
			brick.color = (y + color_offset) % 4 
			add_child(brick)

			total_bricks += 1  


func destroy_brick(brick):
	var bonus = get_bonus()
	if bonus:
		bonus.set_pos(brick.get_pos())
		add_child(bonus)

	brick.queue_free()
	destroy_bricks += 1


func get_count_remaining_brick():
	if total_bricks < destroy_bricks:
		return 0

	return total_bricks -  destroy_bricks


func restart():
	total_bricks = 0
	destroy_bricks = 0
	
	var children = get_children()

	for child in children:
		child.queue_free()

	create()


func _ready():
	screen_rect = get_tree().get_root().get_rect()
	game = get_node("/root/world")
	create()
	