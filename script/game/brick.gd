extends RigidBody2D

const COLOR_RED = 0
const COLOR_GREEN = 1
const COLOR_YELLOW = 2
const COLOR_ORANGE = 3

var colors = [Color(153.0/255, 21.0/255, 13.0/255), Color(60.0/255, 198.0/255, 24.0/255), Color(208.0/255, 198.0/255, 24.0/255), Color(208.0/255, 110.0/255, 24.0/255)]
var color = COLOR_RED

var field = null
var game

func _ready():
	field = get_node("/root/world/field")
	game = get_node("/root/world")
	get_node("spr").set_modulate(colors[color])

func kill():
	field.destroy_brick(self)
	game.add_score(10)
