const S_WAIT = 1
const S_PLAY = 2
const S_GAME_OVER = 3

var bat = null
var ball_spawn = null
var field = null

var state = S_WAIT 
var score = 0
var life = 3

var lab_score = null

func _init():
	add_user_signal("change_life", ["count"])

func add_life(life):
	self.life += life
	if life > 0:
		emit_signal("change_life", self.life)


func dec_life():
	if self.life <= 0:
		return

	self.life -= 1
	emit_signal("change_life", self.life)

	if self.life == 0:
		game_over()
	else:
		restart()


func game_over():
	state = S_GAME_OVER


func start_game():
	score = 0
	life = 3
	emit_signal("change_life", self.life)
	restart()


func next_level():
	bat.restart()
	ball_spawn.restart()
	field.restart()


func restart():
	state = S_WAIT
	bat.restart()
	ball_spawn.restart()
	apply_score()


func is_game_play():
	return state == S_PLAY


func _process(dt):
	if field.get_count_remaining_brick() == 0:
		next_level()


func add_score(v):
	score += v
	apply_score()


func apply_score():
	lab_score.set_text("Score: " + str(score)) 

func _ready():
	bat = get_node("bat")
	ball_spawn = get_node("ball_spawn")
	field = get_node("field")
	lab_score = get_node("score")

	set_process_input(true)
	set_process(true)
	start_game()


func _input(event):
	if state == S_PLAY:
		return

	if !event.is_pressed():
		return

	var touch = false

	if event.type == InputEvent.SCREEN_TOUCH:
		touch = true
	elif event.type == InputEvent.MOUSE_BUTTON:
		touch = true
	elif event.type == InputEvent.KEY:
		touch = true
		
	if touch:
		if state == S_WAIT:
			state = S_PLAY
		elif state == S_GAME_OVER:
			start_game()
