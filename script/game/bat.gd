extends RigidBody2D

export var left_right_offset = 32

var spawn
var screen_rect = null
var game = null
var left = 0
var right = 0

var len = 56.0

const BASE_LEN = 42.0
const REAL_LEN = 4


func _ready():
	screen_rect = get_tree().get_root().get_rect()
	right = screen_rect.size.width - left_right_offset
	left = left_right_offset
	
	spawn = get_pos()
	game = get_node("/root/world")

	set_process_input(true)
	len = BASE_LEN
	apply_len()


func _input(ev):
	if !game || !game.is_game_play():
		return

	if ev.type == InputEvent.MOUSE_MOTION:
		move(ev.relative_x)

func calc_border():
	var left = get_node("left")
	var right = get_node("right")
	
	var l_width = left.get_region_rect().size.width
	var r_width = right.get_region_rect().size.width
	
	self.left = abs(left.get_pos().x) + l_width / 2;
	self.right = screen_rect.size.width - (right.get_pos().x + r_width / 2);


func apply_len():
	var left = get_node("left")
	var right = get_node("right")
	var center = get_node("center")
	var s = len / REAL_LEN
	
	var l_width = left.get_region_rect().size.width
	var r_width = right.get_region_rect().size.width
	var l_x = (len/2+l_width/2)*-1
	var r_x = len/2+r_width/2
	
	center.set_scale(Vector2(s,1))
	left.set_pos(Vector2(l_x, 0))
	right.set_pos(Vector2(r_x, 0))
	var shape = RectangleShape2D.new()
	shape.set_extents(Vector2((len+l_width+r_width)/2, center.get_region_rect().size.height/2))
	remove_shape(0)
	add_shape(shape)
	
	calc_border()


func clamp_pos(pos):
	if pos.x < left:
		pos.x = left
	elif pos.x > right:
		pos.x = right
	
	return pos


func move(delta_x):
	var pos = get_pos()
	pos.x += delta_x
	pos = clamp_pos(pos)
	set_pos(pos)


func inc_bat():
	if len < BASE_LEN * 4:
		len *= 2
		apply_len();
		var pos = clamp_pos(get_pos())
		set_pos(pos)


func dec_bat():
	if len > BASE_LEN / 2:
		len /= 2
		apply_len();
		var pos = clamp_pos(get_pos())
		set_pos(pos)


func restart():
	len = BASE_LEN
	apply_len()
	set_pos(spawn)
