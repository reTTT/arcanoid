extends RigidBody2D

var time = 5

func _on_bonus_body_enter( body ):
	if body.is_in_group("bat"):
		var balls = get_tree().get_nodes_in_group("ball")
		for ball in balls:
			ball.set_punching_blocks(time)
		
		queue_free()
	
