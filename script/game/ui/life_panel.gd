extends HBoxContainer

var game = null

var inst = preload("res://prefab/game/life.xscn")

func _on_change_life(count):
	var children = get_children()
	for child in children:
		 remove_child(child)

	for i in range(count):
		var life = inst.instance()
		add_child(life)


func _ready():
	game = get_node("/root/world")
	game.connect("change_life", self, "_on_change_life")

