extends RigidBody2D

export var inc = true

func _on_bonus_body_enter( body ):
	if body.is_in_group("bat"):
		if inc:
			body.inc_bat()
		else:
			body.dec_bat()
		queue_free()
