extends RigidBody2D

var spawn
var game = null

var base_speed = 500
var max_speed = 700
var inc_speed = 2

var speed = 250
var direction = Vector2(1, -1)
var body_enter = false

var last_contact_shape = null
var last_pos = null
var elapsed_time = 0

var punching_blocks_timer = 0

var last_col = -1

func vector_reflect(v, n):
	return combine_vector(v, n, 1, -2*v.dot(n))


func combine_vector(v1, v2, f1, f2):
	var r = Vector2(0, 0)
	r.x = (f1*v1.x) + (f2*v2.x)
	r.y = (f1*v1.y) + (f2*v2.y)
	return r


func set_punching_blocks(time):
	if punching_blocks_timer < time:
		punching_blocks_timer = time


func _contact(s, i):
	var body = s.get_contact_collider_object(i)
	if body.is_in_group("brick"):
		if punching_blocks_timer <= 0:
			var dp = s.get_contact_local_normal(i)
			direction = vector_reflect(direction, dp)
		body.kill()
	elif body.is_in_group("bat"):
		var ball_pos = get_global_pos()
		var bat_pos = body.get_global_pos()
		direction = (ball_pos - bat_pos).normalized()
	elif body.is_in_group("wall"):
		var dp = s.get_contact_local_normal(i)
		direction = vector_reflect(direction, dp)


func _integrate_forces(s):
	if !game || !game.is_game_play():
		return

	s.set_angular_velocity(0)
	
	#for i in range(s.get_contact_count()):
	if s.get_contact_count() > 0:
		var i = 0
		if s.get_contact_collider_id(i) != last_col:
			_contact(s, i)
			last_col = s.get_contact_collider_id(i)
	
	var lv = direction*speed
	s.set_linear_velocity(lv)


func _process(dt):
	if !game || !game.is_game_play():
		return

	if punching_blocks_timer > 0:
		punching_blocks_timer -= dt


func _ready():
	spawn = get_pos()
	game = get_node("/root/world")
	direction = direction.normalized()
	restart()
	set_process(true)


func restart():
	speed = base_speed
	set_pos(spawn)
	set_linear_velocity(Vector2(0, 0))
	last_pos = spawn
	direction = Vector2(1, -1)
	punching_blocks_timer = 0


func _on_ball_body_enter( body ):
	body_enter = true
	speed = clamp(speed+inc_speed, base_speed, max_speed)
