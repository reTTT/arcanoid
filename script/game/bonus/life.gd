extends RigidBody2D

export var life = 1

func _on_bonus_body_enter( body ):
	if body.is_in_group("bat"):
		var game = get_node("/root/world")
		if life > 0:
			game.add_life(life)
		else:
			game.dec_life()
		queue_free() 
