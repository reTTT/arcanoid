extends Node

var inst = preload("res://prefab/game/ball.xscn")

var game = null

func restart():
	for child in get_children():
		child.queue_free()
	
	var ball = inst.instance()
	ball.set_pos(Vector2(240, 600))
	add_child(ball)

func _process(delta):
	if get_child_count() == 0:
		game.dec_life()
		
	
func _ready():
	game = get_node("/root/world")
	restart()
	set_process(true)
